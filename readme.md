# Overview

A laravel app that verifies a user's account number and BVN using Glade APIs.

A user on entering their account number and selecting bank, sees their account name.
Also, on entering their BVN, their other details are fetched and displayed on the form.

The app is hosted on an AWS EC2 instance (i.e. the production environment)
[http://ec2-54-157-189-218.compute-1.amazonaws.com/](http://ec2-54-157-189-218.compute-1.amazonaws.com/)

The app also has a staging environment set up on Heroku
[https://glade-verify-account-bvn.herokuapp.com/](https://glade-verify-account-bvn.herokuapp.com/)

App repository is on Gitlab
[https://gitlab.com/kevchikezie/glade-account-bvn-verification](https://gitlab.com/kevchikezie/glade-account-bvn-verification)

Gitlab CI was also used for auto-deployment to both the staging and production server based
on the branch to which a **git push** is done.

A little PHPUnit test was also written.
