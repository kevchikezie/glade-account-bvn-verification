<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'glade' => [
        'key' => env('GLADE_MERCHANT_KEY'),
        'mid' => env('GLADE_MERCHANT_ID'),
        'env' => $gladeEnv = env('GLADE_ENV', 'testing'), // 'testing', 'live'
        'url' => ($gladeEnv === 'live')
                    ? env('GLADE_LIVE_URL', 'https://api.glade.ng')
                    : env('GLADE_TEST_URL', 'https://demo.api.gladepay.com'),
    ],

];
