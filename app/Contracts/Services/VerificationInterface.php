<?php

namespace App\Contracts\Services;

interface VerificationInterface
{
    /**
     * Fetch list of banks
     *
     * @return array
     */
    public function getBanks();

    /**
     * Verify account name using account number
     *
     * @param  array  $data
     * @return array
     */
    public function verifyAccountName(array $data);

    /**
     * Verify user's Bank Verification Number (BVN)
     *
     * @param  string  $bvn
     * @return array
     */
    public function verifyBvn(string $bvn);
}
