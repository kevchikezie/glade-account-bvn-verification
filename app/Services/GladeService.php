<?php

namespace App\Services;

use App\Contracts\Services\VerificationInterface;

class GladeService implements VerificationInterface
{
	/**
	 * Get the list of banks
	 *
	 * @return array
	 */
	public function getBanks()
	{
		return $this->sendRequest('resources', [
			"inquire" => "banks",
		]);
	}

	/**
     * Verify account name using account number
     *
     * @param  array  $data
     * @return array
     */
	public function verifyAccountName(array $data)
	{
		return $this->sendRequest('resources', [
    		"inquire" => "accountname",
    		"accountnumber" => $data['account_number'],
    		"bankcode" => $data['bank_name'],
    	]);
	}

	/**
     * Verify user's Bank Verification Number (BVN)
     *
     * @param  string  $bvn
     * @return array
     */
    public function verifyBvn(string $bvn)
    {
    	return $this->sendRequest('resources', [
    		"inquire" => "bvn",
    		"bvn" => $bvn,
    	]);
    }

	/**
	 * Send request to Glade API using cURL
	 *
	 * @param  string  $endpoint
	 * @param  array  $payload
	 * @return array
	 */
	public function sendRequest($endpoint, array $payload)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->getUrl($endpoint),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => json_encode($payload),
			CURLOPT_HTTPHEADER => array(
				"key: " .config('services.glade.key'),
				"mid: " .config('services.glade.mid'),
			),
		));

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			// die("cURL Error #:" . $err); // TODO: Log error
			return [];
		} else {
			return json_decode($response, true);
		}
	}

	/**
     * Get the Glade API root URL, then attach the endpoint a request
     * is to be made to and return a full url for the request
     *
     * @param  string|null  $endpoint
     * @return string
     */
    public function getUrl($endpoint = null)
    {
    	// Remove multiple forward slashes at the beginning of the endpoint
    	$endpoint = preg_replace('/(\/+)/', '/', $endpoint);
    	// Ensure there is at least one forward slash at the beginning of the endpoint
    	$endpoint = substr($endpoint, 0, 1) !== '/' ? '/'.$endpoint : $endpoint;
    	// Sanitize the endpoint
    	$endpoint = filter_var($endpoint, FILTER_SANITIZE_URL);

    	return config('services.glade.url') . $endpoint;
    }
}