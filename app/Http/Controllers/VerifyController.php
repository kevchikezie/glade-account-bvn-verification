<?php

namespace App\Http\Controllers;

use App\Contracts\Services\VerificationInterface as VerificationService;
use App\Http\Requests\VerifyAccountNumberRequest;
use App\Http\Requests\VerifyBvnRequest;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
	/**
     * Properties
     *
     * @property
     */
	private $verificationService;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct(VerificationService $verificationService)
	{
		$this->verificationService = $verificationService;
	}

	/**
	 * Verify account name using account number
	 *
	 * @param  \App\Http\Requests\VerifyAccountNumberRequest  $request
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
    public function accountName(VerifyAccountNumberRequest $request)
    {
    	$accountName = $this->verificationService->verifyAccountName($request->toArray());

    	return response()->json($accountName);
    }

    /**
	 * Verify user's Bank Verification Number (BVN)
	 *
	 * @param  \App\Http\Requests\VerifyBvnRequest  $request
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
    public function bvn(VerifyBvnRequest $request)
    {
    	$accountDetails = $this->verificationService->verifyBvn($request->bvn);

    	return response()->json($accountDetails);
    }
}
