<?php

namespace App\Http\Controllers;

use App\Contracts\Services\VerificationInterface as VerificationService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
	/**
     * Properties
     *
     * @property
     */
	private $verificationService;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct(VerificationService $verificationService)
	{
		$this->verificationService = $verificationService;
	}

	/**
	 * Display the form for verification
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function create()
	{
		$banks = $this->verificationService->getBanks();

		return view('home', compact('banks'));
	}
}
