<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name') }}</title>
  <style>
  body { font-family: sans-serif;  }

  label {
    font-size: 14px;
    font-weight: 600;
    margin-top: 12px;
    display: block;
  }

  input[type=text], input[type=number], select {
    width: 100%;
    padding: 10px 18px;
    margin: 4px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=button] {
    width: 100%;
    background-color: #00afef;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  input[type=submit]:hover {
    background-color: #3498db;
  }

  div {
    border-radius: 5px;
    background-color: rgba(0, 175, 239, 0.03);
    padding: 20px;
  }

  .form {
    width: 50%;
    margin: 0 auto;
  }

  @media only screen and (max-width: 630px) {
    .form {
      width: 100%;
    }
  }
  </style>
</head>
<body>


<div>
  <form action="#" method="POST" class="form">
    <label for="bank_name">Bank Name</label>
    <select id="bank_name" name="bank_name">
      <option value="">Select A Bank</option>
      @foreach ($banks as $key => $bank)
        <option value="{{ $key }}" {{ old('bank_name') == $key ? 'selected' : '' }}>{{ $bank }}</option>
      @endforeach
    </select>
    <span class="accountNumberError" style="display: block; color: red; font-size: 12px;"></span>

    <label for="account_number">Account Number</label>
    <input type="text" name="account_number" placeholder="E.g 0292837365" value="{{ old('account_number') }}" maxlength="10" id="account_number">
    <span class="accountNumberError" style="display: block; color: red; font-size: 12px;"></span>

    <label for="bvn">Your BVN</label>
    <input type="text" name="bvn" placeholder="E.g 21345678901" value="{{ old('bvn') }}" maxlength="11" id="bvn">
    <span class="bvnError" style="display: block; color: red; font-size: 12px;"></span>

    <label for="account_number">Account Name</label>
    <input type="text" name="account_name" placeholder="E.g Joe Ken Dan" id="account_name" readonly>

    <label for="account_number">First Name</label>
    <input type="text" name="first_name" placeholder="E.g Joe" id="first_name" readonly>

    <label for="account_number">Last Name</label>
    <input type="text" name="last_name" placeholder="E.g Dan" id="last_name" readonly>

    <label for="account_number">Date of birth</label>
    <input type="text" name="dob" placeholder="E.g 1945-05-13" id="dob" readonly>

    <label for="account_number">Phone Number</label>
    <input type="text" name="phone" placeholder="E.g 08012345678" id="phone" readonly>

    <input type="button" value="Submit">
  </form>
</div>

<script src="{{ asset('assets/lib/jquery/jquery.min.js') }}"></script>

<script>
  $(document).ready(function () {
    $('#bvn').on('change', function () {
      verifyBvn(this.value);
    });

    $('#account_number').on('change', function () {
      let bankName = $('#bank_name').val();
      let accountNumber = $('#account_number').val();

      if (bankName != '' && accountNumber != '') {
        verifyAccountName(bankName, accountNumber);
      }
    });

    $('#bank_name').on('change', function () {
      let bankName = $('#bank_name').val();
      let accountNumber = $('#account_number').val();

      if (bankName != '' && accountNumber != '') {
        verifyAccountName(bankName, accountNumber);
      }
    });

  });

  function verifyAccountName(bank_name, account_number) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      type: 'POST',
      url: `/verify/account-name`,
      data: {
        bank_name,
        account_number
      },
      beforeSend : function() {
        $('.accountNumberError').text('');
        $('#account_name').val('');
      },
      success: function(response) {
        if (response.status === 'success') {
          $('#account_name').val(response.data.account_name);
        }

        if (response.status === 'error') {
          $('.accountNumberError').text(response.error);
        }
      },
      error: function(error) {
        $('.accountNumberError').text('');
        $('.accountNumberError').text(error.responseJSON.message);
      }
    });
  }

  function verifyBvn(bvn) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      type: 'POST',
      url: `/verify/bvn`,
      data: {
        bvn
      },
      beforeSend : function() {
        $('.bvnError').text('');
        $('#first_name').val('');
        $('#last_name').val('');
        $('#phone').val('');
        $('#dob').val('');
      },
      success: function(response) {
        if (response.status === 'success') {
          $('#first_name').val(response.data.firstname);
          $('#last_name').val(response.data.lastname);
          $('#phone').val(response.data.phone);
          $('#dob').val(response.data.dob);
        }
      },
      error: function(error) {
        $('.bvnError').text('');
        $('.bvnError').text(error.responseJSON.message);
      }
    });
  }
</script>

</body>
</html>
